#pragma once
#include "OutStream.h"
#include <stdio.h>
class OutStreamEncrypted : public OutStream
{
public:
	//c'tors
	OutStreamEncrypted();
	OutStreamEncrypted& operator<<(char *str);
	//setters
	void setKey(int key);
	void setLen(int len);
	//d'tor
	~OutStreamEncrypted();
private:
	int _key;
	int _len;
};