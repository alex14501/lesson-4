#pragma once
#include <iostream>
class OutStream
{
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE* file));
	FILE * file;
private:
	
};

void endline();