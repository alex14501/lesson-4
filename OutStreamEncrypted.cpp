////////////////////////////////////////////
////Ecrypted outstream class////////////////
////made by alex////////////////////////////
////////////////////////////////////////////
#include "OutStreamEncrypted.h"
#define LAST_ASCII_NUM 126
#define FIRST_ASCII_NUM 32
OutStreamEncrypted::OutStreamEncrypted()
{
}
void OutStreamEncrypted::setKey(int key)
{
	this->_key = key;
}
void OutStreamEncrypted::setLen(int len)
{
	this->_len = len;
}
OutStreamEncrypted& OutStreamEncrypted::operator<<(char *str)
{
	int diffrence = 0;
	for (int i = 0; i < this->_len; i++)
	{
		if (str[i] + this->_key >= LAST_ASCII_NUM)//checking if char+key is biiger then ascii limit
		{
			diffrence = LAST_ASCII_NUM - str[i];//calculating how much needed to added after the ascii limit
			str[i] = (FIRST_ASCII_NUM + (this->_key - diffrence));
		}
		else
		{
			if (str[i] + this->_key <= FIRST_ASCII_NUM)//checking if char+key is smaller then ascii limit
			{
				diffrence = str[i] - FIRST_ASCII_NUM;
				str[i] = (LAST_ASCII_NUM - (this->_key - diffrence));
			}
			else
			{
				str[i] = str[i]+char(this->_key);
			}
		}
	}
	OutStream::operator<<(str);
	return *this;
}
OutStreamEncrypted::~OutStreamEncrypted()
{

}