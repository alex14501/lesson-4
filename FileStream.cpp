#include "FileStream.h"
FileStream::FileStream()
{
	this->file = fopen("file.txt", "r+");
}
FileStream::FileStream(char* path)
{
	this->file = fopen(path, "w");
}
FileStream::~FileStream()
{
	fclose(this->file);
}