//////////////////////////////////////////////////////////////////
///out stream class(adapted for File out streaming)///////////////
///made by alex///////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
#include "OutStream.h"

OutStream::OutStream()
{
	this->file = stdout;//for use to print to the screen
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->file,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->file,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* file))
{
	pf(this->file);
	return *this;
}


void endline(FILE* file)
{
	fprintf(file,"\n");
}
